#include <forecast/Hardware.hpp>

// configurations
#include <forecast/config/AB_encoder_environment.h>
#include <forecast/config/AB_encoder_motor.h>
#include <forecast/config/analog_torque_sensor.h>
#include <forecast/config/escon_motor.h>
#include <forecast/config/escon_motor_env.h>
#include <forecast/config/spring.h>

forecast::Status forecast::Hardware::init() {
    if (not motorEncoderInit())
        return Status::MOTOR_ENCODER_INIT_ERR;

    if (not envEncoderInit())
        return Status::ENV_ENCODER_INIT_ERR;

    if (not motorControlInit())
        return Status::CONTROL_MOTOR_INIT_ERR;
    control_motor->setTorque(0.f);

    if (not motorEnvironmentInit())
        return Status::ENV_MOTOR_INIT_ERR;
    env_motor->setTorque(0.f);

    if (not torqueSensorInit())
        return Status::TORQUE_SENSOR_INIT_ERR;
    
    tau_m_offset = 0.0f;
    for (uint16_t i = 0; i < 1000; i++)
    {
        wait_ms(5);
        tau_m_offset += torque_sensor->read_average_float();
    }
    tau_m_offset /= 1000.0f;
    tau_m_offset *= 3.3f;
        
    return Status::NO_ERROR;
}

bool forecast::Hardware::motorEncoderInit() {
    encoder_motor =
        new DigitalEncoderAB(motorEncoder::CPR, motorEncoder::GEAR_RATIO);

    /* Set the encoder timer */
    encoder_motor->setTIM1();//read position
    encoder_motor->setTIM3();//read velocity
    
    return true;
}

bool forecast::Hardware::envEncoderInit() {
    encoder_env = new DigitalEncoderAB(envEncoder::CPR, envEncoder::GEAR_RATIO);

    /* Set the encoder timer */
    encoder_env->setTIM8();//read position
    encoder_env->setTIM4();//read velocity

    return true;
}

bool forecast::Hardware::motorControlInit() {
    MotorConfiguration conf;
    conf.enable = motorControl::MOTOR_ENABLE_PIN;
    conf.currFeedback = motorControl::MOTOR_CURRENT_FEEDBACK_PIN;
    conf.analog = motorControl::MOTOR_ANALOG_PIN;

    /* Control motor */
    control_motor = new EsconMotor(conf, motorControl::KT, motorControl::JM,
                                   motorControl::MAX_CURR);

    return control_motor != nullptr;
}

bool forecast::Hardware::motorEnvironmentInit() {
    MotorConfiguration conf;
    conf.enable = envMotor::MOTOR_ENABLE_PIN;
    conf.currFeedback = envMotor::MOTOR_CURRENT_FEEDBACK_PIN;
    conf.analog = envMotor::MOTOR_ANALOG_PIN;

    /* Environment motor */
    env_motor =
        new EsconMotor(conf, envMotor::KT, envMotor::JM, envMotor::MAX_CURR);

    return env_motor != nullptr;
}

bool forecast::Hardware::torqueSensorInit() {
    torque_sensor =
        new AnalogInput(TORQUE_SENSOR_PIN, ADC_PCLK2, ADC_Right, ADC_15s,
                        ADC_12b, ADC_Continuous, ADC_Dma, BUFFER_SIZE);

    /* Enable the ADC - In continous mode the ADC start is done automatically */
    auto enabled = torque_sensor->enable();

    return enabled == -1 ? false : true;
}

// void forecast::Hardware::update(float controlTorque,
//                                 float envTorque,
//                                 float dt) {
void forecast::Hardware::update(float dt) {
    /* Time update */
    this->dt = dt;
    t = us_ticker_read() / 1e6;

    /* Motor encoder update */
    /* Motor encoder is mounted in the opposite position wrt env encoder */
    theta_m.val = - encoder_motor->getAngleRad();
    // theta_m.dval = (theta_m.val - prev_theta_m.val) / dt;
    theta_m.dval = encoder_motor->getVelocityRad(dt);
    theta_m.ddval = (theta_m.dval - prev_theta_m.dval) / dt;
    prev_theta_m.val = theta_m.val;
    prev_theta_m.dval = theta_m.dval;

    /* Environment encoder update */
    theta_e.val = encoder_env->getAngleRad();
    // theta_e.dval = (theta_e.val - prev_theta_e.val) / dt;
    theta_e.dval = encoder_env->getVelocityRad(dt);
    theta_e.ddval = (theta_e.dval - prev_theta_e.dval) / dt;
    prev_theta_e.val = theta_e.val;
    prev_theta_e.dval = theta_e.dval;

    /* Control motor update  (from torque sensor) */
    // +10 -10 Volt
    // tau_m.val = 
    //     ((torque_sensor->read_average_float() * 3.3f) - tau_m_offset) * 7.6628f;
    // +3.3 -3.3 Volt
    tau_m.val = 
        ((torque_sensor->read_average_float() * 3.3f) - tau_m_offset) * 7.333333333f;
    tau_m.dval = (tau_m.val - prev_tau_m.val) / dt;
    tau_m.ddval = (tau_m.dval - prev_tau_m.dval) / dt;
    prev_tau_m.val = tau_m.val;
    prev_tau_m.dval = tau_m.dval;

    /* Environment motor update  (from Escon feedback) */
    envTorqueFeedback.val = env_motor->getTorqueFeedback();
    envTorqueFeedback.dval =
        (envTorqueFeedback.val - prev_envTorqueFeedback.val) / dt;
    envTorqueFeedback.ddval =
        (envTorqueFeedback.dval - prev_envTorqueFeedback.dval) / dt;
    prev_envTorqueFeedback.val = envTorqueFeedback.val;
    prev_envTorqueFeedback.dval = envTorqueFeedback.dval;

    /* Spring torque update */
    tau_s.val = KS * (theta_m.val - theta_e.val);
    tau_s.dval = (tau_s.val - prev_tau_s.val) / dt;
    tau_s.ddval = (tau_s.dval - prev_tau_s.dval) / dt;
    prev_tau_s.val = tau_s.val;
    prev_tau_s.dval = tau_s.dval;

}
