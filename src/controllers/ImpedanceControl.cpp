#include <forecast/controllers/ImpedanceControl.hpp>

using namespace forecast;

ImpedanceControl::ImpedanceControl()
    : Controller(5),
      theta(0.f),
      theta_eq(0.f),
      err(0.f),
      tau_ref(0.0f),
      once(1) {

    lowPass = utility::AnalogFilter::getDifferentiatorHz(30.0f);

}

ImpedanceControl::ImpedanceControl(float kp, float kd, float k_des, float b_des, float j_des)
    : Controller(5),
      kp(kp),
      kd(kd),
      k_des(k_des),
      theta(0.f),
      theta_eq(0.f),
      err(0.f),
      once(1) {

    lowPass = utility::AnalogFilter::getDifferentiatorHz(30.0f);

    Controller::initialized = true;
}

bool ImpedanceControl::init(const std::vector<float> &params) {
    
    if (params.size() != numberOfParams)
        return false;
    
    kp = params[0];
    kd = params[1];
    k_des = params[2];
    b_des = params[3];
    j_des = params[4];

    
    out.val = 0.0;
    out.dval = 0.0;
    out.ddval = 0.0;

    once = 1;

    return initialized = true;
}

utility::ddvar ImpedanceControl::process(const Hardware* hw, utility::ddvar ref) {
    
    /* Get the equilibrium state */
    if(once) {
        theta_eq = hw->getThetaE().val;
        once = 0;
    }

    float tau = -hw->getTauM().val;
    float dtau = -hw->getTauM().dval;

    ddtheta_filt = lowPass->process(hw->getThetaE().dval, hw->getDT());

    err = theta_eq - hw->getThetaE().val;
    derr = hw->getThetaE().dval;
    dderr = ddtheta_filt;

    tau_ref = k_des * err + b_des * derr + j_des * dderr;

    out.val = kp * (tau_ref - tau) + kd * dtau + tau_ref;

    return out;
}

std::vector<std::string> ImpedanceControl::getParamNames() const {
    return {"KP", "KD", "K_DES", "B_DES", "J_DES" };
}
