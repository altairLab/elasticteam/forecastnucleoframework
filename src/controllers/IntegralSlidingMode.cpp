#include <forecast/controllers/IntegralSlidingMode.hpp>

#define __sign(x) (std::signbit(x)?-1.0:1.0)

using namespace forecast;

IntegralSlidingModeForceControl::IntegralSlidingModeForceControl()
    : Controller(4),
    s(0.0),
    ni(0.0), phi(0.0), ka(0.0),
    err(0.0), ierr(0.0), derr(0.0), dderr(0.0), err_prev(0.0),
    ddtheta_e(0.0) {
        // ntd
    }

IntegralSlidingModeForceControl::IntegralSlidingModeForceControl(float lambda1, float lambda2, float Jm, float Ks)
    : Controller(4),   
    lambda1(lambda1), lambda2(lambda2), 
    Ks(Ks), Jm(Jm), A(Jm / Ks),
    kp(A * lambda2 - 1), 
    kd(A * lambda1), 
    s(0.0),
    ni(0.0), phi(0.0), ka(0.0),
    err(0.0), ierr(0.0), derr(0.0), dderr(0.0), err_prev(0.0),
    ddtheta_e(0.0) {
    Controller::initialized = true;
}


bool IntegralSlidingModeForceControl::init(const std::vector<float>& params) {
    if (params.size() != numberOfParams)
        return false;

    lambda1 = params[0];
    lambda2 = params[1];
    Ks = params[2];
    Jm = params[3];

    A = Jm/Ks;
    kp = A * lambda2 - 1;
    kd = A * lambda1;

    return initialized = true;
}

utility::ddvar IntegralSlidingModeForceControl::process(const Hardware* hw,
                                   utility::ddvar ref)
{
    err = hw->getTauM().val - ref.val;
    derr = hw->getTauM().dval - ref.dval;

    // SLIDING SURFACE
    if(fabs(s) > 2*phi) 
        ierr = -err/lambda1; //to avoid the reaching phase
    else if (__sign(err) != __sign(err_prev)) 
        ierr = 0;
    ierr += err*hw->getDT();
	s = derr + lambda1*err + lambda2 * ierr;

    // CONTROL LAW
	us = __sign(s);
	if(fabs(s) < phi) us =  s / phi;
    // ddtheta_e = ddtheta_m - ddtau / Ks;
    out.val = - kp * err - kd * derr + A*ref.ddval + ref.val + ka*Jm*ddtheta_e - ni*us;
    
    err_prev = err;

    return out;
}

void IntegralSlidingModeForceControl::set_ddtheta(float ddtheta){
    this -> ddtheta_e = ddtheta;
}

std::vector<std::string> IntegralSlidingModeForceControl::getParamNames() const {
    return {"LAMBDA1", "LAMBDA2", "KS", "JM"};
}