#include <forecast/controllers/ForceDOB_OL3.hpp>

using namespace forecast;

ForceDOB_OL3::ForceDOB_OL3()
    : Controller(4), errPast(0.f), err(0.f), derr(0.f), ierr(0.f) {
    // ntd
}

ForceDOB_OL3::ForceDOB_OL3(float kp, float ki, float kd, float QcutoffHz)
    : Controller(4),
      kp(kp),
      ki(ki),
      kd(kd),
      QcutoffHz(QcutoffHz),
      errPast(0.f),
      err(0.f),
      derr(0.f),
      ierr(0.f) {
    Controller::initialized = true;
}

bool ForceDOB_OL3::init(const std::vector<float>& params) {
    if (params.size() != numberOfParams)
        return false;

    kp = params[0];
    ki = params[1];
    kd = params[2];
    QcutoffHz = params[3];

    w_q = 2 * M_PI * QcutoffHz;
    xi_q = 1.4f;

    // Q filter
    double a_Q[3] = {1.0, 2 * xi_q * w_q, pow(w_q, 2)};
    double b_Q[3] = {0.0, 0.0, pow(w_q, 2)};

    Q_filter = new utility::AnalogFilter(2, a_Q, b_Q);

    // QPninvQ filter

    float Jel = 0.001f;
    float del = 0.35f;
    float Jm = motorControl::JM;
    float dm = motorControl::DM;
/*
    b = w_q^2 * {Jm * Jel, Jm * del + Jel * dm, dm * del + KS * (Jm + Jel),  KS * (dm + del)};
    a = KS * {Jel, del + 2 * xi_q * w_q * Jel , w_q * (w_q * Jel + 2 * xi_q * del), w_q * w_q * del};
*/
    double a_PninvQ[4] = {KS * Jel, KS * (del + 2 * xi_q * w_q * Jel), KS * w_q * (w_q * Jel + 2 * xi_q * del), KS * pow(w_q, 2) * del};
    double b_PninvQ[4] =  {pow(w_q,2) * Jm * Jel, pow(w_q,2) * (Jm * del + Jel * dm), pow(w_q,2) * (dm * del + KS * (Jm + Jel)), pow(w_q,2) * (KS * (dm + del))};

    PninvQ_filter = new utility::AnalogFilter(3, a_PninvQ, b_PninvQ);

    // FF filter
    FF_filter = new utility::AnalogFilter(3, a_PninvQ, b_PninvQ);


    return initialized = true;
}

utility::ddvar ForceDOB_OL3::process(const Hardware* hw, utility::ddvar ref) {

    err = ref.val - hw->getTauS().val;
    derr = (err - errPast) / hw->getDT();
    ierr += err * hw->getDT();

    //fare con FF dinamico 
    //out.val = ref.val + kp * err + kd * derr + ki * ierr;
    out.val = FF_filter->process(ref.val,hw->getDT()) + kp * err + kd * derr + ki * ierr;
    d1 = Q_filter->process(u_prev, hw->getDT());
    d2 = PninvQ_filter->process(hw->getTauS().val, hw->getDT());
    d_hat = d2 - d1;
    out.val -= d_hat;
    u_prev = out.val;

    errPast = err;

    return out;
}

std::vector<std::string> ForceDOB_OL3::getParamNames() const {
    return {"KP", "KI", "KD", "QcutoffHz"};
}