#include <forecast/controllers/MegaPD.hpp>


using namespace forecast;

MegaPD::MegaPD()
    : Controller(3),
      err(0.f),
      derr(0.f) {
    // ntd
}

MegaPD::MegaPD(float kp, float kd, float ff)
    : Controller(3),
      kp(kp),
      kd(kd),
      ff(ff),
      err(0.f),
      derr(0.f) {
    Controller::initialized = true;
}

bool MegaPD::init(const std::vector<float>& params) {
    if (params.size() != numberOfParams)
        return false;

    kp = params[0];
    kd = params[1];
    ff = params[2];

    return initialized = true;
}

utility::ddvar MegaPD::process(const Hardware* hw, utility::ddvar ref)
{   
    auto tau = hw->getTauS();
    err = ref.val - tau.val;
    derr = ref.dval - tau.dval;
    
    out.val = ff * ref.val + err * kp + derr * kd;

    return out;

}

std::vector<std::string> MegaPD::getParamNames() const {
    return {"KP", "KD", "FF"};
}