#include <forecast/controllers/ForceDOB_CL3.hpp>

using namespace forecast;

ForceDOB_CL3::ForceDOB_CL3()
    : Controller(4), errPast(0.f), err(0.f), derr(0.f), ierr(0.f) {
    // ntd
}

ForceDOB_CL3::ForceDOB_CL3(float kp, float ki, float kd, float QcutoffHz)
    : Controller(4),
      kp(kp),
      ki(ki),
      kd(kd),
      QcutoffHz(QcutoffHz),
      errPast(0.f),
      err(0.f),
      derr(0.f),
      ierr(0.f) {
    Controller::initialized = true;
}

bool ForceDOB_CL3::init(const std::vector<float>& params) {
    if (params.size() != numberOfParams)
        return false;

    kp = params[0];
    ki = params[1];
    kd = params[2];
    QcutoffHz = params[3];

    w_q = 2 * M_PI * QcutoffHz;

    // Q filter
    double a_Q[2] = {1, w_q};
    double b_Q[2] = {0.0, w_q};

    Q_filter = new utility::AnalogFilter(1, a_Q, b_Q);

    // QPninvQ filter

    float Jel = 0.001f;
    float del = 0.35f;
    float Jm = motorControl::JM;
    float dm = motorControl::DM;
    FF = 1+(dm/del);
/*
    a = KS * [Jel * kd, Jel * (FF + kp + w_q * kd) + del * kd, w_q * (Jel * FF + Jel * kp + del * kd) + del * (FF + kp), w_q * del * (FF + kp)];
    b = w_q * [FF * Jm * Jel, FF * (Jm * del + Jel * dm) + KS * Jel * kd, FF * (Jm * KS + dm * del + Jel * KS) + KS * (Jel * kp + del * kd), KS * (FF * (dm + del) + del * kp)];
*/
    // double a_PninvQ[4] = {KS * Jel * kd, KS * (Jel * (FF + kp + w_q * kd) + del * kd), KS * (w_q * (Jel * FF + Jel * kp + del * kd) + del * (FF + kp)), KS * w_q * del * (FF + kp)};
    // double b_PninvQ[4] =  {w_q * FF * Jm * Jel, w_q * (FF * (Jm * del + Jel * dm) + KS * Jel * kd), w_q * (FF * (Jm * KS + dm * del + Jel * KS) + KS * (Jel * kp + del * kd)), w_q *(KS * (FF * (dm + del) + del * kp))};

    double a_PninvQ[4] = {KS * Jel * kd, KS * (Jel * (FF + kp + w_q * kd) + del * kd), KS * (w_q * (Jel * FF + Jel * kp + del * kd) + del * (FF + kp)), KS * w_q * del * (FF + kp)};
    double b_PninvQ[4] =  {w_q * Jm * Jel, w_q * ( (Jm * del + Jel * dm) + KS * Jel * kd), w_q * (dm * del + KS * (Jm + Jel + Jel * kp + del * kd)), w_q *(KS * (dm + del + del * kp))};


    PninvQ_filter = new utility::AnalogFilter(3, a_PninvQ, b_PninvQ);

    return initialized = true;
}

utility::ddvar ForceDOB_CL3::process(const Hardware* hw, utility::ddvar ref) {
    
    refd = ref.val - prev_d_hat;
    err = refd - hw->getTauS().val;
    derr = (err - errPast) / hw->getDT();
    

    out.val = refd + kp * err + kd * derr ;


    d1 = Q_filter->process(refd, hw->getDT());
    d2 = PninvQ_filter->process(hw->getTauS().val, hw->getDT());
    d_hat = d2 - d1;
    prev_d_hat = d_hat;

    errPast = err;

    return out;
}

std::vector<std::string> ForceDOB_CL3::getParamNames() const {
    return {"KP", "KI", "KD", "QcutoffHz"};
}