#include <forecast/controllers/ForceDOB_CL.hpp>

using namespace forecast;

ForceDOB_CL::ForceDOB_CL()
    : Controller(4), errPast(0.f), err(0.f), derr(0.f), ierr(0.f) {
    // ntd
}

ForceDOB_CL::ForceDOB_CL(float kp, float ki, float kd, float QcutoffHz)
    : Controller(4),
      kp(kp),
      ki(ki),
      kd(kd),
      QcutoffHz(QcutoffHz),
      errPast(0.f),
      err(0.f),
      derr(0.f),
      ierr(0.f) {
    Controller::initialized = true;
}

bool ForceDOB_CL::init(const std::vector<float>& params) {
    if (params.size() != numberOfParams)
        return false;

    kp = params[0];
    ki = params[1];
    kd = params[2];
    QcutoffHz = params[3];

    w_q = 2 * M_PI * QcutoffHz;
    float k_spring = 12.34f;
    

    // Q filter
    double a_Q[2] = {1.0,w_q};
    double b_Q[2] = {0.0, w_q};
    Q_filter = new utility::AnalogFilter(1, a_Q, b_Q);

    // QPninvQ filter
    double a_PninvQ[3] = {kd, kp + 1 + (w_q * kd), (kp + 1) * w_q};
    double b_PninvQ[3] = {w_q* (motorControl::JM / k_spring), w_q*(((motorControl::DM) / k_spring) + (kd)), (1 + kp)*w_q };

    PninvQ_filter = new utility::AnalogFilter(2, a_PninvQ, b_PninvQ);

    return  initialized = true;
}

utility::ddvar ForceDOB_CL::process(const Hardware* hw, utility::ddvar ref) {

    
    
    
    Rd = ref.val - prev_d_hat;
    


    err = Rd - hw->getTauS().val;
    derr = (err - errPast) / hw->getDT();
    


    out.val = ref.val + kp * err + kd * derr ;
    d1 = Q_filter->process(Rd, hw->getDT());
    d2 = PninvQ_filter->process(hw->getTauS().val, hw->getDT());
    d_hat = d2 - d1;
    

    errPast = err;
    prev_d_hat = d_hat;


    return out;
}

std::vector<std::string> ForceDOB_CL::getParamNames() const {
    return {"KP", "KI", "KD", "QcutoffHz"};
}