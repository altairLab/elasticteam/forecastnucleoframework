#include <forecast/controllers/ForceDOB_OL.hpp>

using namespace forecast;

ForceDOB_OL::ForceDOB_OL()
    : Controller(4), errPast(0.f), err(0.f), derr(0.f), ierr(0.f) {
    // ntd
}

ForceDOB_OL::ForceDOB_OL(float kp, float ki, float kd, float QcutoffHz)
    : Controller(4),
      kp(kp),
      ki(ki),
      kd(kd),
      QcutoffHz(QcutoffHz),
      errPast(0.f),
      err(0.f),
      derr(0.f),
      ierr(0.f) {
    Controller::initialized = true;
}

bool ForceDOB_OL::init(const std::vector<float>& params) {
    if (params.size() != numberOfParams)
        return false;

    kp = params[0];
    ki = params[1];
    kd = params[2];
    QcutoffHz = params[3];

    w_q = 2 * M_PI * QcutoffHz;
    xi_q = 1.4f; //0.704f;

    // Q filter
    double a_Q[3] = {1.0, 2 * xi_q * w_q, pow(w_q, 2)};
    double b_Q[3] = {0.0, 0.0, pow(w_q, 2)};
    Q_filter = new utility::AnalogFilter(2, a_Q, b_Q);

    // QPninvQ filter
    double a_PninvQ[3] = {1 / pow(w_q, 2), 2 * xi_q / w_q, 1.0};
    double b_PninvQ[3] = { motorControl::JM/12.34f , motorControl::DM / 12.34f, 1.0f };

    PninvQ_filter = new utility::AnalogFilter(2, a_PninvQ, b_PninvQ);

    return initialized = true;
}

utility::ddvar ForceDOB_OL::process(const Hardware* hw, utility::ddvar ref) {

    err = ref.val - hw->getTauS().val;
    derr = (err - errPast) / hw->getDT();
    ierr += err * hw->getDT();

    out.val = ref.val + kp * err + kd * derr + ki * ierr;
    d1 = Q_filter->process(u_prev, hw->getDT());
    d2 = PninvQ_filter->process(hw->getTauS().val, hw->getDT());
    d_hat = d2 - d1;
    out.val -= d_hat;
    u_prev = out.val;

    errPast = err;

    return out;
}

std::vector<std::string> ForceDOB_OL::getParamNames() const {
    return {"KP", "KI", "KD", "QcutoffHz"};
}