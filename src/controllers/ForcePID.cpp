#include <forecast/controllers/ForcePID.hpp>

using namespace forecast;

ForcePID::ForcePID()
    : Controller(3), errPast(0.f), err(0.f), derr(0.f), ierr(0.f) {
    // ntd
}

ForcePID::ForcePID(float kp, float ki, float kd)
    : Controller(3),
      kp(kp),
      ki(ki),
      kd(kd),
      errPast(0.f),
      err(0.f),
      derr(0.f),
      ierr(0.f) {
    Controller::initialized = true;
}

bool ForcePID::init(const std::vector<float>& params) {
    if (params.size() != numberOfParams)
        return false;

    kp = params[0];
    ki = params[1];
    kd = params[2];

    return initialized = true;
}

utility::ddvar ForcePID::process(const Hardware* hw, utility::ddvar ref) {

    err = ref.val - hw->getTauS().val;
    derr = (err - errPast) / hw->getDT();
    ierr += err * hw->getDT();

    out.val = ref.val + kp * err + kd * derr + ki * ierr;

    errPast = err;

    // Velocity Control
    // err = ref.val - hw->getThetaM().dval;
    // derr = (err - errPast) / hw->getDT();
    // ierr += err * hw->getDT();
    // out.val = kp * err + kd * derr + ki * ierr;
    // // out.val = 0.3*sin(2*M_PI*1*(hw->getT()));

    // errPast = err;

    return out;
}

std::vector<std::string> ForcePID::getParamNames() const {
    return {"KP", "KI", "KD"};
}