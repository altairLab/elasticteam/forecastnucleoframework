#ifndef EXO_HW_HPP
#define EXO_HW_HPP

#include <mbed.h>

#include <hw/DigitalEncoder/DigitalEncoderAB.h>
#include <hw/Motor/EsconMotor.h>
#include <utility/math.hpp>

#include "Status.hpp"

namespace forecast {
class Hardware {
   public:
    /**
     * @brief   Hardware constructor
     */
    Hardware() = default;

    // ~Hardware() {
    //     delete encoder;
    //     delete motor;
    //     delete strain_gauge;
    // }

    /**
     * @brief   Initialization of the Hardware
     */
    Status init();

    /**
     * @brief   Return the hw time t of the last update.
     *
     * @return  t
     */
    virtual inline float getT() const { return t; }
    /**
     * @brief   Return the hw dt used in the last update.
     *
     * @return  dt
     */
    virtual inline float getDT() const { return dt; }

    /**std::make_unique<control::Control>()
     * @brief   Return the torque applied by the motor (measured by the sensor
     * torque)
     *
     * @return  motorTorqueFeedback
     */
    virtual inline utility::ddvar getTauM() const { return tau_m; }

    /**
     * @brief   Return the torque applied by the environment (current feedback)
     *
     * @return  envTorqueFeedback
     */
    virtual inline utility::ddvar getTauE() const { return envTorqueFeedback; }

    /**
     * @brief   Return the torque measured by the spring
     *
     * @return  tau_s
     */
    virtual inline utility::ddvar getTauS() const { return tau_s; }

    /**
     * @brief   Return the angle radius measured by the encoder of the motor.
     *
     * @return  theta_m
     */
    virtual inline utility::ddvar getThetaM() const { return theta_m; }

    /**
     * @brief   Return the angle radius measured by the encoder of the
     * environment.
     *
     * @return  theta_e
     */
    virtual inline utility::ddvar getThetaE() const { return theta_e; }

    virtual inline utility::ddvar getOutput() const { return output; }

    /**
     * @brief   Update the Hardware by reading the value from the physical hw
     *
     * @param   Torque that has to be given to the motor for actuating the
     * control Torque that has to be given to the environment for the simulation
     *
     * @param   dt is the delta time in seconds to use for the
     * calculations for controls
     */
    // void update(float controlTorque, float envTorque, float dt);
    void update(float dt);

    inline void enableControlMotor() { control_motor->setEnable(true); }

    inline void enableEnvMotor() { env_motor->setEnable(true); }

    inline void disableControlMotor() { control_motor->setEnable(false); }

    inline void disableEnvMotor() { env_motor->setEnable(false); }


    // inline void resetEncoder() { encoder_offset = encoder->getAngleRad(); }
    inline void setControlTorque(float torque) {
        control_motor->setTorque(torque);
    }

    inline void setEnvTorque(float torque) { env_motor->setTorque(torque); }

   protected:
    bool motorEncoderInit();  /// < Initialize the motor encoder

    bool envEncoderInit();  /// < Initialize the environment encoder

    bool motorControlInit();  ///< Initialize the motor

    bool motorEnvironmentInit();  ///< Initialize the motor

    bool torqueSensorInit();  ///< Initialize the motor torque sensor

    DigitalEncoderAB* encoder_motor = nullptr;  ///< Motor encoder
    DigitalEncoderAB* encoder_env = nullptr;    ///< Environment encoder

    EsconMotor* control_motor = nullptr;  ///< Motor used for the control
    EsconMotor* env_motor = nullptr;  ///< Motor used for the env. simulation

    AnalogInput* torque_sensor = nullptr;  ///< Motor torque sensor

    float t, dt;

    float tau_m_offset;

    utility::ddvar tau_m;              ///< tau_motor
    utility::ddvar envTorqueFeedback;  ///< tau_environment
    utility::ddvar tau_s;              ///< tau_spring (or elastic link)
    utility::ddvar theta_m;            ///< motor position
    utility::ddvar theta_e;            ///< environment position

    utility::ddvar prev_tau_m;
    utility::ddvar prev_envTorqueFeedback;
    utility::ddvar prev_tau_s;
    utility::ddvar prev_theta_m;
    utility::ddvar prev_theta_e;

    utility::ddvar output;
};
}  // namespace forecast

#endif  // EXO_HW_HPP