#include <USART/USART_interface.hpp>

#include "Controller.hpp"
#include "Hardware.hpp"
#include "Status.hpp"
#include "com.hpp"

#include <functional>
#include <memory>
#include <vector>

namespace forecast {

using LoggerCallback = std::function<
    std::vector<float>(utility::ddvar, const Hardware*, const Controller*, Controller*)>;
using RefGen = std::function<utility::ddvar(const Hardware*)>;

class App {
   private:
    // need of an instance of forecastHW created by the app
    Hardware hw;
    std::unique_ptr<Controller> environment;
    std::unique_ptr<Controller> motor;

    LoggerCallback logger;
    Status status;
    size_t cycleCount;
    bool handshake;

    RefGen envRefGen;
    RefGen motorRefGen;

    USART_interface pc;
    const size_t txBuffSz;
    const size_t rxBuffSz;

   public:
    App(unsigned int txBuffSz = 256,
        unsigned int rxBuffSz = 256,
        unsigned int baudrate = 921600);

    void setEnviorment(std::unique_ptr<Controller> env);
    void setMotor(std::unique_ptr<Controller> motor);
    void setEnviorment(Controller* env);
    void setMotor(Controller* motor);
    void setLogger(LoggerCallback logger);
    void setEnvRefGen(RefGen gen);
    void setMotorRefGen(RefGen gen);

    /**
     * @brief Checks if the App is ready to run
     *
     * @return true if the object is ready to exec the control
     * @return false if the object is not ready to exec the control loop
     */
    bool isOk();

    /**
     * @brief Execs the function execControl at the control frequency.
     *
     * @param[in] freq The frequency of the loop in Hz. Default to 1 KHz
     */
    bool execControlLoop(ulong freq = 1000);

    /**
     * @brief Execs one cicle of the control loop.
     *
     * This method should be called at a specified frequency to garantee to have
     * a good control.
     *
     */
    bool execControl();

    /**
     * @brief It logs the values returned by the logger callback function
     *
     */
    void logInfo();

    /**
     * @brief It logs an error through the serial
     *
     */
    void logError(const std::string& msg);

    //==========================communication===================================

    /**
     * @brief Requires the parameters for the motor controller from the pc
     * interface
     *
     */
    bool requireMotorParams();

    /**
     * @brief Requires the parameters for the enviorment controller from the pc
     * interface
     *
     */
    bool requireEnvironmentParams();

    /**
     * @brief Requires a simple float value for the user, if a name is passed
     * this will be showed to the user.
     *
     * @param valueName
     * @return float
     */
    float requireFloatValue(const std::string& valueName = "");

    /**
     * @brief Send a message to the pc.
     *
     * It is a time expensive function to not use for logging functionalities.
     *
     * @param msg Message to send.
     */
    void sendMessage(const std::string& msg);

    /**
     * @brief This method will wait for a message from the pc to be sure that
     * the connection is established
     *
     */
    bool waitConnection(); 

   private:
    void sendData(com::Type t, const void* data, size_t sz, bool wait = false);
    ssize_t receiveData(com::Type& t, void* buff, size_t maxSz = 0u);
    std::vector<float> requireParams(const std::unique_ptr<Controller>& ctrl,
                                     const std::string& ctrlName);
};

}  // namespace forecast