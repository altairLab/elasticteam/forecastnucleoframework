#ifndef SLIDING_MODE_CONTROL_H
#define SLIDING_MODE_CONTROL_H

#include "../Controller.hpp"

namespace forecast {
class IntegralSlidingModeForceControl : public Controller {
    public:
    /**
     * @brief Integral Sliding Mode control constructor
     **/
    IntegralSlidingModeForceControl();

    /**
     * @brief Construct a new Integral Sliding Mode object. This constructor initialize, 
     * the controller.
     * 
     * @param lambda1
     * @param lambda2
     * @param Jm
     * @param Ks
     */
    IntegralSlidingModeForceControl(float lambda1, float lambda2, float Jm, float Ks);

    /**
     * @brief Integral Sliding Mode control initialization
     *
     * @param a vector containing the constants lambda1, lambda2, Jm, Ks
     * in the respected order
     **/
    virtual bool init(const std::vector<float>& params) override;

    /**
     * @brief Get Return the names of the parameters.
     * 
     * @return std::vector<std::string> 
     */
    virtual std::vector<std::string> getParamNames() const override;

    virtual utility::ddvar process(const Hardware* hw,
                                   utility::ddvar ref) override;
    void set_ddtheta(float ddtheta);  // Read from the accelerometer

   protected:
    float lambda1, lambda2;
    float Ks, Jm, A;
    float kp, kd;       // TO READ
    float s;
    float ni, phi, ka;  // TO SET
    float err, ierr, derr, dderr, err_prev;
    float ddtheta_e;  // Required for acceleration feedback
    float us;
    utility::ddvar out;
};
}  // namespace forecast

#endif  // SLIDING_MODE_CONTROL_H
