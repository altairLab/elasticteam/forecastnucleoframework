#ifndef FORCE_PID_H
#define FORCE_PID_H

#include "../Controller.hpp"

namespace forecast {

/**
 * @brief ForcePID control class
 **/

class ForcePID : public Controller {
   public:
    /**
     * @brief PID Force control constructor
     *
     * @param pointer to the HW interface
     **/
    ForcePID();

    /**
     * @brief Construct a new Force P I D object. This constructor initialize, 
     * the controller.
     * 
     * @param kp 
     * @param ki 
     * @param kd 
     */
    ForcePID(float kp, float ki, float kd);

    /**
     * @brief PID control initialization
     *
     * @param a vector containing the constants P, I, D
     * with the order respect
     **/
    virtual bool init(const std::vector<float>& params) override;

    /**
     * @brief Get Return the names of the parameters.
     * 
     * @return std::vector<std::string> 
     */
    virtual std::vector<std::string> getParamNames() const override;

    virtual utility::ddvar process(const Hardware* hw,
                                   utility::ddvar ref) override;

   protected:
    float kp = 0.0;
    float ki = 0.0;
    float kd = 0.0;

    float errPast = 0.0;

    float err = 0.0;
    float derr = 0.0;
    float ierr = 0.0;

    utility::ddvar out;
};
}  // namespace forecast

#endif  // FORCE_PID_H