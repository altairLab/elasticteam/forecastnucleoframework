#ifndef MEGA_PD_H
#define MEGA_PD_H

#include "../Controller.hpp"

namespace forecast {
/**
 * @brief MegaPD control class
 **/
class MegaPD : public Controller {
    public:
    /**
     * @brief MegaPD control constructor
     **/
    MegaPD();

    /**
     * @brief Construct a new MegaPD object. This constructor initialize, 
     * the controller.
     * 
     * @param kp
     * @param kd
     * @param ff
     */
    MegaPD(float kp, float kd, float ff);

    /**
     * @brief MegaPD control initialization
     *
     * @param a vector containing the constants kpd, kd, ff
     * in the respected order
     **/
    virtual bool init(const std::vector<float>& params) override;

    /**
     * @brief Get Return the names of the parameters.
     * 
     * @return std::vector<std::string> 
     */
    virtual std::vector<std::string> getParamNames() const override;

    virtual utility::ddvar process(const Hardware* hw,
                                   utility::ddvar ref) override;

    protected:
    float kp = 0.0;
    float kd = 0.0;
    float ff = 0.0;
    float err = 0.0;
    float derr = 0.0;
    utility::ddvar out;
};

}
#endif // MEGA_PD_H