#ifndef IMPEDANCE_CONTROL_H
#define IMPEDANCE_CONTROL_H

#include "../Controller.hpp"
#include "../config/spring.h"
#include <utility/filters/AnalogFilter.hpp>

namespace forecast {

/**
 * @brief Impedance Control class 
**/
class ImpedanceControl : public Controller { 

    public:
    /**
     * @brief Impedance control constructor
    **/
    ImpedanceControl();
    /**
     * @brief Construct a new ImpedanceControl object. This constructor initialize, 
     * the controller.
     * @param kp
     * @param kd
     * @param k_des
     * @param b_des
     * @param j_des

    **/
    ImpedanceControl(float kp, float kd, float k_des, float b_des, float j_des);
    
    /**
     * @brief Impedance control initialization 
     * 
     * @param a vector containing the constants KP , K_DES
     * in this order
    **/
    virtual bool init(const std::vector<float> &params) override;
    /**
     * @brief Get Return the names of the parameters.
     * 
     * @return std::vector<std::string> 
     */
    virtual std::vector<std::string> getParamNames() const override;

    virtual utility::ddvar process(const Hardware* hw, utility::ddvar ref) override;

    float theta_eq = 0.0f;
    
    protected:

    float kp = 0.0;
    float kd = 0.0;
    float k_des = 0.0;
    float b_des = 0.0f;
    float j_des = 0.0f;
    
    float tau_ref = 0.0f;
    float theta = 0.0; 

    float ddtheta_filt = 0.0f;
    float err = 0.0;
    float derr = 0.0;
    float dderr = 0.0;

    uint8_t once = 1;

    utility::ddvar out;

    utility::AnalogFilter *lowPass;

};
}

#endif // IMPEDANCE_CONTROL_H

