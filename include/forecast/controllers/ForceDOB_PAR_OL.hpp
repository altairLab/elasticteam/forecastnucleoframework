#ifndef FORCE_DOB_PAR_OL_H
#define FORCE_DOB_PAR_OL_H

#include "../Controller.hpp"
#include <utility/filters/AnalogFilter.hpp>
#include <forecast/config/escon_motor.h>

namespace forecast {

/**
 * @brief ForceDOB_PAR_OL control class
 **/

class ForceDOB_PAR_OL : public Controller {
   public:
    /**
     * @brief PID+DOB Partial OL Force control constructor
     *
     * @param pointer to the HW interface
     **/
    ForceDOB_PAR_OL();

    /**
     * @brief Construct a new Force P I D + DOB_OL object. This constructor initialize, 
     * the controller.
     * 
     * @param kp 
     * @param ki 
     * @param kd 
     * @param QcutoffHz
     */
    ForceDOB_PAR_OL(float kp, float ki, float kd, float QcutoffHz);

    /**
     * @brief DOB_OL control initialization
     *
     * @param a vector containing the constants P, I, D
     * with the order respect
     **/
    virtual bool init(const std::vector<float>& params) override;

    /**
     * @brief Get Return the names of the parameters.
     * 
     * @return std::vector<std::string> 
     */
    virtual std::vector<std::string> getParamNames() const override;

    virtual utility::ddvar process(const Hardware* hw,
                                   utility::ddvar ref) override;

   protected:
    float kp = 0.0;
    float ki = 0.0;
    float kd = 0.0;
    float QcutoffHz = 0.0;

    float errPast = 0.0;

    float err = 0.0;
    float derr = 0.0;
    float ierr = 0.0;

    float w_q, xi_q, d1, d2, d_hat;
    float u_prev = 0.0f;
    float u = 0.0f;
    float theta_prev = 0.0f;

    utility::AnalogFilter* Q_filter;
    utility::AnalogFilter* PninvQ_filter;

    utility::ddvar out;
};
}  // namespace forecast

#endif  // FORCE_DOB_PAR_OL_H