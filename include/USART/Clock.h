#ifndef CLOCK_H
#define CLOCK_H

    #ifdef TARGET_STM32L4
        #include "stm32l4xx.h"
    #elif TARGET_STM32F4
        #include "stm32f4xx.h"
    #endif

    #ifndef HSE_CLOCK
        #define HSE_CLOCK 0
    #endif

    static float get_pll_source_clock(){
        switch (RCC->PLLCFGR & RCC_PLLCFGR_PLLSRC)
        {
            case RCC_PLLCFGR_PLLSRC_HSI:
                return 16000000.0f;

            case RCC_PLLCFGR_PLLSRC_HSE:
                return (float)HSE_CLOCK;

            default:
                return 0.0f;
        }
    }

    static float get_m(){
        return ((float)((RCC->PLLCFGR & RCC_PLLCFGR_PLLM)>>RCC_PLLCFGR_PLLM_Pos));
    }

    static float get_n(){
        return ((float)((RCC->PLLCFGR & RCC_PLLCFGR_PLLN)>>RCC_PLLCFGR_PLLN_Pos));
    }

    static float get_p(){
        switch (RCC->PLLCFGR & RCC_PLLCFGR_PLLP)
        {
            case 0:
                return (2.0f);

            case 1:
                return (4.0f);

            case 2:
                return (6.0f);

            case 3:
                return (8.0f);

            default:
                return (1.0f);
        }
    }

    static float get_q(){
        return ((float)((RCC->PLLCFGR & RCC_PLLCFGR_PLLQ)>>RCC_PLLCFGR_PLLQ_Pos));
    }

    static float get_r(){
        return ((float)((RCC->PLLCFGR & RCC_PLLCFGR_PLLR)>>RCC_PLLCFGR_PLLR_Pos));
    }

    static float get_pllclk(){
        return ((get_pll_source_clock()*(get_n()/get_m()))/get_p());
    }

    static float get_pllq(){
        return ((get_pll_source_clock()*(get_n()/get_m()))/get_q());
    }

    static float get_pllr(){
        return ((get_pll_source_clock()*(get_n()/get_m()))/get_r());
    }

    static float get_pllsaip(){
        //M3 /P
        return 0.0f;
    }

    static float get_pllsaiq(){
        //M3 /Q DIV
        return 0.0f;
    }

    static float get_plli2sp(){
        //M2 /P
        return 0.0f;
    }

    static float get_plli2s_saiclk(){
        //M2 /Q DIV
        return 0.0f;
    }

    static float get_plli2sr(){
        //M2 /R
        return 0.0f;
    }

    static float get_system_clock(){
        switch(RCC->CFGR & RCC_CFGR_SWS)
        {
            case RCC_CFGR_SWS_HSI:
                return 16000000.0f;
            
            case RCC_CFGR_SWS_HSE:
                return (float)HSE_CLOCK;
            
            case RCC_CFGR_SWS_PLL:
                return get_pllclk();
            
            #ifdef TARGET_STM32F4
                case RCC_CFGR_SWS_PLLR:
                    return get_pllr();
            #endif

            default:
                return 0.0f;
        }
    }

    static float get_ahb_clock(){
        float P = 0.0f;
        switch(RCC->CFGR & RCC_CFGR_HPRE)
        {
            case RCC_CFGR_HPRE_DIV1:
                P = 1.0;
                break;

            case RCC_CFGR_HPRE_DIV2:
                P = 2.0;
                break;

            case RCC_CFGR_HPRE_DIV4:
                P = 4.0;
                break;
            
            case RCC_CFGR_HPRE_DIV8:
                P = 8.0;
                break;
            
            case RCC_CFGR_HPRE_DIV16:
                P = 16.0;
                break;
            
            case RCC_CFGR_HPRE_DIV64:
                P = 64.0;
                break;
            
            case RCC_CFGR_HPRE_DIV128:
                P = 128.0;
                break;
            
            case RCC_CFGR_HPRE_DIV256:
                P = 256.0;
                break;
            
            case RCC_CFGR_HPRE_DIV512:
                P = 512.0;
                break;
            
            default:
                P = 1.0;
        }
        return (get_system_clock()/P);
    }

    static float get_apb1_clock(){
        float P1 = 0.0f;
        switch(RCC->CFGR & RCC_CFGR_PPRE1)
        {
            case RCC_CFGR_PPRE1_DIV1:
                P1 = 1.0;
                break;

            case RCC_CFGR_PPRE1_DIV2:
                P1 = 2.0;
                break;

            case RCC_CFGR_PPRE1_DIV4:
                P1 = 4.0;
                break;
            
            case RCC_CFGR_PPRE1_DIV8:
                P1 = 8.0;
                break;
            
            case RCC_CFGR_PPRE1_DIV16:
                P1 = 16.0;
                break;
            
            default:
                P1 = 1.0;
        }
        return (get_ahb_clock()/P1);
    }

    static float get_apb2_clock(){
        float P2 = 0.0f;
        switch(RCC->CFGR & RCC_CFGR_PPRE2)
        {
            case RCC_CFGR_PPRE2_DIV1:
                P2 = 1.0;
                break;

            case RCC_CFGR_PPRE2_DIV2:
                P2 = 2.0;
                break;

            case RCC_CFGR_PPRE2_DIV4:
                P2 = 4.0;
                break;
            
            case RCC_CFGR_PPRE2_DIV8:
                P2 = 8.0;
                break;
            
            case RCC_CFGR_PPRE2_DIV16:
                P2 = 16.0;
                break;
            
            default:
                P2 = 1.0;
        }
        return (get_ahb_clock()/P2);
    }

    static uint32_t get_usart_clock(USART_TypeDef *usart)
    {
        #ifdef TARGET_STM32L4
            if (usart == USART1)
            {
                switch(RCC->CCIPR & RCC_CCIPR_USART1SEL)
                {
                    case RCC_CCIPR_USART1SEL:
                        return LSE_VALUE;
                        break;
                    
                    case RCC_CCIPR_USART1SEL_1:
                        return HSI_VALUE;
                    
                    case RCC_CCIPR_USART1SEL_0:
                        return (uint32_t)get_system_clock();
                    
                    default:
                        return (uint32_t)get_apb2_clock();
                }
            }
            else if (usart == USART2)
            {
                switch(RCC->CCIPR & RCC_CCIPR_USART2SEL)
                {
                    case RCC_CCIPR_USART2SEL:
                        return LSE_VALUE;
                        break;
                    
                    case RCC_CCIPR_USART2SEL_1:
                        return HSI_VALUE;
                    
                    case RCC_CCIPR_USART2SEL_0:
                        return (uint32_t)get_system_clock();
                    
                    default:
                        return (uint32_t)get_apb1_clock();
                }
            }
            return (0.0f);
        #elif TARGET_STM32F4
            if (usart == USART1) return (uint32_t)get_apb2_clock();
            else if (usart == USART2) return (uint32_t)get_apb1_clock();
            else if (usart == USART3) return (uint32_t)get_apb1_clock();
            else if (usart == USART6) return (uint32_t)get_apb2_clock();
            return (0.0f);
        #endif
    }

#endif //CLOCK_H