import sys
import re
import shutil
import os

Import("env")

# including config parser to parse the .ini files
try:
    import configparser
except ImportError:
    import ConfigParser as configparser

# defining important paths
TEMPLATE_DIR = os.path.join("..", "config_templates")
CONFIG_DIR = os.path.join("..", "include/forecast/config")
DEFAULT_CONFIG_PATH = os.path.join(".", "default_config.ini")

# checking for templates
if not os.path.isdir(TEMPLATE_DIR):
    print("The templates directory does not exists!", file=sys.stderr)
    Exit(1)

# checking for the default configs
if not os.path.isfile(DEFAULT_CONFIG_PATH):
    print("Default configs not provided!", file=sys.stderr)
    Exit(2)

# creating the config directory
if os.path.isdir(CONFIG_DIR):
    shutil.rmtree(CONFIG_DIR)

# Opening the default config file
config_def = configparser.ConfigParser()
config_def.read('default_config.ini')

# Looking for the platformio.ini file
pio_init = configparser.ConfigParser()
pio_init_path = os.path.join(env["PROJECT_DIR"], "platformio.ini")
pio_init.read(pio_init_path)

# Looking for the value "forecast_cofig" in the current pioenv
forecast_config = os.path.join(env["PROJECT_DIR"], "forecast_config.ini")
pioenv = "env:" + env["PIOENV"]
if pio_init.has_option(pioenv, "forecast_config"):
    if os.path.isabs(pio_init.get(pioenv, "forecast_config")):
        forecast_config = pio_init.get(pioenv, "forecast_config")
    else:
        forecast_config = \
            os.path.join(env["PROJECT_DIR"], 
                         pio_init.get(pioenv, "forecast_config"))

if not os.path.isfile(forecast_config):
    print("The file \"", forecast_config,
          "\" does not exists!", file=sys.stderr)
    Exit(3)

# Opening the config user as configparser object
config_usr = configparser.ConfigParser()
config_usr.read(forecast_config)

# creating the config directory
os.mkdir(CONFIG_DIR)

template_regex = re.compile("\$(\w+)\[(\w+)\]\$")

# foreach template file 
for file in os.listdir(TEMPLATE_DIR):
    path = os.path.join(TEMPLATE_DIR, file)
    if os.path.isfile(path):
        infile = open(path, 'r')
        content = infile.read()
        infile.close()
        for match in re.finditer(template_regex, content):
            section = ('forecast:' + match.group(1)).strip()
            option = match.group(2).strip()

            # Looking for the value in the config user
            if config_usr.has_option(section, option):
                value = config_usr.get(section, option)
            # Looking for the value in the config default
            elif config_def.has_option(section, option):
                value = config_def.get(section, option)
            # Error if it is not found
            else:
                print("The value:\"", option, "\" of the section \"", section,
                      "\" required by the template: \"", file, "\" ",
                      "is not been declared", file=sys.stderr, sep="")
                Exit(4)
            
            # replacing the value
            content = content.replace(match.group(0), value)

        # writing the config file
        out = open(os.path.join(CONFIG_DIR, file), 'w')
        out.write(content)
        out.close()
